


1. How to set special types file when execute `npm run typegen`

```text
$ npm run typegen -- manifest.yml

> subpoli@0.0.0 typegen
> hydra-typegen typegen "manifest.yml"

2021-11-15 15:26:34        REGISTRY: Unknown signed extensions CheckEthereumRelayHeaderParcel found, treating them as no-effect
2021-11-15 15:26:34        REGISTRY: Unable to resolve type RingBalance, it will fail on construction
    Error: createType(RingBalance):: DoNotConstruct: Cannot construct unknown type RingBalance
```


2. Does one subsquid repository have a support staging/production environment?

3. When the type changes, what actions do I need to do to support the new types.

   The steps I understand are

   - Update types/pangolin.json
   - (cd indexer && docker-compose restart)
   - npm run typegen -- manifest.yml
   - npm run codegen
   - Maybe there need to do db:migration
   - npm run processor:start

4. If indexer is still synchronizing old blocks after the types are changed, will it cause the types to not exist and the synchronization cannot be continued? 

